#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARRSZ 1000U
#define BITWT 12U

int main(void)
{
	unsigned int cal, bi, csr[ARRSZ], ev, i, j, oal, oc, ogr[ARRSZ];
	int ch;
	memset(csr, 0, sizeof(csr));
	memset(ogr, 0, sizeof(ogr));
	for (cal = oal = 0; (ch = fgetc(stdin)) != EOF;) {
		switch (ch) {
		case '0':
			csr[cal] <<= 1U;
			ogr[oal] <<= 1U;
			break;
		case '1':
			csr[cal] |= 0x1U;
			csr[cal] <<= 1U;
			ogr[oal] |= 0x1U;
			ogr[oal] <<= 1U;
			break;
		case '\n':
			csr[cal] >>= 1U;
			++cal;
			ogr[oal] >>= 1U;
			++oal;
			break;
		}
	}
	for (bi = BITWT; bi > 0; --bi) {
		if (cal > 1) {
			oc = 0;
			for (i = 0; i < cal; ++i) {
				if (((csr[i] >> (bi - 1)) & 0x1U) == 0x1U)
					++oc;
			}
			ev = (cal - oc) <= oc ? 0x1 : 0x0;
			j = 0;
			for (i = 0; i < cal; ++i) {
				if (((csr[i] >> (bi - 1)) & 0x1U) == ev)
					continue;
				csr[j++] = csr[i];
			}
			cal = j;
		}
		if (oal > 1) {
			oc = 0;
			for (i = 0; i < oal; ++i) {
				if (((ogr[i] >> (bi - 1)) & 0x1U) == 0x1U)
					++oc;
			}
			ev = (oal - oc) <= oc ? 0x0 : 0x1;
			j = 0;
			for (i = 0; i < oal; ++i) {
				if (((ogr[i] >> (bi - 1)) & 0x1U) == ev)
					continue;
				ogr[j++] = ogr[i];
			}
			oal = j;
		}
	}
	printf("%u\n", csr[0] * ogr[0]);
	return EXIT_SUCCESS;
}
