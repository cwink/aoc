#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	unsigned int ct;
	int pv, cv;
	ct = 0;
	fscanf(stdin, "%d", &pv);
	while (fscanf(stdin, "%d", &cv) != EOF) {
		if (cv > pv)
			++ct;
		pv = cv;
	}
	printf("%d\n", ct);
	return EXIT_SUCCESS;
}
