#include <stdio.h>
#include <stdlib.h>

#define POSSZ 1024

#define ABS(X) ((X) < 0 ? ((X) * -1) : (X))

int main(void)
{
	unsigned int cf, j, pl, tcf;
	int i, mn, mx, pos[POSSZ];
	mn = ~((unsigned int)0);
	mx = 0;
	for (pl = 0; fscanf(stdin, "%d ,", &pos[pl]) != EOF; ++pl) {
		if (pos[pl] < mn)
			mn = pos[pl];
		if (pos[pl] > mx)
			mx = pos[pl];
	}
	cf = ~((unsigned int)0);
	for (i = mn; i < mx; ++i) {
		tcf = 0;
		for (j = 0; j < pl; ++j)
			tcf += ABS(i - pos[j]);
		if (tcf < cf)
			cf = tcf;
	}
	printf("%u\n", cf);
	return EXIT_SUCCESS;
}
