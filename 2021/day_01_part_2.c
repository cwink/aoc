#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	unsigned int ct;
	int v1, v2, v3, vsc, vsp;
	fscanf(stdin, "%d", &v1);
	fscanf(stdin, "%d", &v2);
	fscanf(stdin, "%d", &v3);
	vsp = v1 + v2 + v3;
	v1 = v2;
	v2 = v3;
	ct = 0;
	while (fscanf(stdin, "%d", &v3) != EOF) {
		if ((vsc = v1 + v2 + v3) > vsp)
			++ct;
		vsp = vsc;
		v1 = v2;
		v2 = v3;
	}
	printf("%d\n", ct);
	return EXIT_SUCCESS;
}
