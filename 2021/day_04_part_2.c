#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BRDSSZ 100
#define BUFSZ  512
#define NUMSSZ 128

static int  bcheck(void);
static void bset(const int val);
static int  bsum(const int brd);
static void ginput(void);

static unsigned int bl, nl, wins[BRDSSZ];
static int          brds[BRDSSZ][5][5], nums[NUMSSZ];

static int
bcheck(void)
{
	unsigned int i, x, y;
	for (i = 0; i < bl; ++i) {
		if (wins[i])
			continue;
		for (y = 0; y < 5; ++y)
			if (brds[i][y][0] == -1 && brds[i][y][1] == -1 && brds[i][y][2] == -1 && brds[i][y][3] == -1 && brds[i][y][4] == -1)
				return i;
		for (x = 0; x < 5; ++x)
			if (brds[i][0][x] == -1 && brds[i][1][x] == -1 && brds[i][2][x] == -1 && brds[i][3][x] == -1 && brds[i][4][x] == -1)
				return i;
	}
	return -1;
}

static void
bset(const int val)
{
	unsigned int i, x, y;
	for (i = 0; i < bl; ++i) {
		if (wins[i])
			continue;
		for (y = 0; y < 5; ++y)
			for (x = 0; x < 5; ++x)
				if (brds[i][y][x] == val)
					brds[i][y][x] = -1;
	}
}

static int
bsum(const int brd)
{
	unsigned int x, y;
	int rv;
	rv = 0;
	for (y = 0; y < 5; ++y)
		for (x = 0; x < 5; ++x)
			if (brds[brd][y][x] != -1)
				rv += brds[brd][y][x];
	return rv;
}

static void
ginput(void)
{
	unsigned int i;
	char buf[BUFSZ], *tok;
	memset(wins, 0, sizeof(wins));
	fgets(buf, sizeof(buf), stdin);
	tok = strtok(buf, ",");
	nums[0] = atoi(tok);
	for (nl = 1; (tok = strtok(NULL, ",")) != NULL; ++nl)
		nums[nl] = atoi(tok);
	for (bl = 0; ; ++bl)
		for (i = 0; i < 5; ++i)
			if (fscanf(stdin, "%d %d %d %d %d", &brds[bl][i][0], &brds[bl][i][1], &brds[bl][i][2], &brds[bl][i][3], &brds[bl][i][4]) == EOF)
				return;
}

int main(void)
{
	unsigned int i;
	int res, rv;
	ginput();
	for (i = 0; i < 4; ++i)
		bset(nums[i]);
	for (; i < nl; ++i) {
		bset(nums[i]);
		while ((rv = bcheck()) >= 0) {
			res = nums[i] * bsum(rv);
			wins[rv] = 1;
		}
	}
	printf("%d\n", res);
	return EXIT_SUCCESS;
}
