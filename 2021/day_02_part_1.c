#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	int dp, hp, un;
	char dir[8];
	hp = dp = 0;
	while (fscanf(stdin, "%s %d", dir, &un) != EOF) {
		switch (dir[0]) {
		case 'f':
			hp += un;
			break;
		case 'd':
			dp += un;
			break;
		case 'u':
			dp -= un;
			break;
		}
	}
	printf("%d\n", hp * dp);
	return EXIT_SUCCESS;
}
