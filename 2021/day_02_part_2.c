#include <stdio.h>
#include <stdlib.h>

int main(void)
{
	int am, dp, hp, un;
	char dir[8];
	am = hp = dp = 0;
	while (fscanf(stdin, "%s %d", dir, &un) != EOF) {
		switch (dir[0]) {
		case 'f':
			hp += un;
			dp += am * un;
			break;
		case 'd':
			am += un;
			break;
		case 'u':
			am -= un;
			break;
		}
	}
	printf("%d\n", hp * dp);
	return EXIT_SUCCESS;
}
