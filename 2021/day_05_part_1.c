#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LNSSZ 512
#define PTSSZ 512000

#define MIN(X, Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X, Y) ((X) > (Y) ? (X) : (Y))

typedef struct {
	unsigned int c[PTSSZ];
	int          x[PTSSZ], y[PTSSZ];
} Pnts;

static void pnts_init(Pnts *pts);
static void pnts_set(Pnts *pts, int x, int y);

static void
pnts_init(Pnts *pts)
{
	memset(pts->c, 0, sizeof(pts->c));
	memset(pts->x, -1, sizeof(pts->x));
	memset(pts->y, -1, sizeof(pts->y));
}

static void
pnts_set(Pnts *const pts, const int x, const int y)
{
	unsigned int hs;
	for (hs = ((((unsigned long)x << 32U) & 0xFFFFFFFF00000000U) | ((unsigned long)y & 0xFFFFFFFFU)) % PTSSZ; pts->x[hs] != -1 && !(pts->x[hs] == x && pts->y[hs] == y); hs = (hs + 1) % PTSSZ)
		continue;
	if (pts->x[hs] == -1) {
		pts->x[hs] = x;
		pts->y[hs] = y;
	}
	++pts->c[hs];
}

int main(void)
{
	Pnts pts;
	unsigned int ct, i, j, pl;
	int m, x1[LNSSZ], x2[LNSSZ], y1[LNSSZ], y2[LNSSZ];
	for (pl = 0; (fscanf(stdin, "%d , %d -> %d , %d", &x1[pl], &y1[pl], &x2[pl], &y2[pl])) != EOF; ++pl)
		if (x1[pl] != x2[pl] && y1[pl] != y2[pl])
			--pl;
	pnts_init(&pts);
	for (i = 0; i < pl; ++i) {
		if (x1[i] == x2[i]) {
			m = MAX(y1[i], y2[i]);
			for (j = MIN(y1[i], y2[i]); j <= (unsigned int)m; ++j)
				pnts_set(&pts, x1[i], j);
		} else if (y1[i] == y2[i]) {
			m = MAX(x1[i], x2[i]);
			for (j = MIN(x1[i], x2[i]); j <= (unsigned int)m; ++j)
				pnts_set(&pts, j, y1[i]);
		}
	}
	ct = 0;
	for (i = 0; i < PTSSZ; ++i)
		if (pts.c[i] > 1)
			++ct;
	printf("%u\n", ct);
	return EXIT_SUCCESS;
}
