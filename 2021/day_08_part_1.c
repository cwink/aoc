#include <stdio.h>
#include <stdlib.h>

#define SGCT0 6
#define SGCT1 2
#define SGCT2 5
#define SGCT3 5
#define SGCT4 4
#define SGCT5 5
#define SGCT6 5
#define SGCT7 3
#define SGCT8 7
#define SGCT9 6

int main(void)
{
	unsigned int cnt, i;
	int bl, ch;
	char buf[10];
	cnt = 0;
	while ((ch = fgetc(stdin)) != EOF) {
		if (ch != '|')
			continue;
		for (i = 0; i < 4; ++i) {
			fscanf(stdin, "%10s%n", buf, &bl);
			--bl;
			switch (bl) {
			case SGCT1: /* FALLTHROUGH */
			case SGCT4: /* FALLTHROUGH */
			case SGCT7: /* FALLTHROUGH */
			case SGCT8: /* FALLTHROUGH */
				++cnt;
				break;
			}
		}
	}
	printf("%u\n", cnt);
	return EXIT_SUCCESS;
}
