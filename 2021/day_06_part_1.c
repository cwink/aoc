#include <stdio.h>
#include <stdlib.h>

#define DAYS  80
#define FSTSZ 1024000

int main(void)
{
	unsigned int ds, i, nf, nwf;
	unsigned char fst[FSTSZ];
	for (nf = 0; fscanf(stdin, "%u ,", &i) != EOF; ++nf)
		fst[nf] = i & 0xFFU;
	for (ds = 0; ds < DAYS; ++ds) {
		nwf = 0;
		for (i = 0; i < nf; ++i) {
			if (fst[i] != 0) {
				--fst[i];
				continue;
			}
			fst[i] = 6;
			++nwf;
		}
		for (i = 0; i < nwf; ++i) {
			fst[nf++] = 8;
		}
	}
	printf("%u\n", nf);
	return EXIT_SUCCESS;
}
