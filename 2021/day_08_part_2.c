#include <stdio.h>
#include <stdlib.h>

/*
 ####
#    #
#    #
 ####
#    #
#    #
 ####

to

 aaaa
b    c
b    c
 dddd
e    f
e    f
 gggg

to

aaaabcbcddddefefgggg
*/

static inline void dcfgsa(char *dis, char a);
static inline void dcfgsb(char *dis, char b);
static inline void dcfgsc(char *dis, char c);
static inline void dcfgsd(char *dis, char d);
static inline void dcfgse(char *dis, char e);
static inline void dcfgsf(char *dis, char f);
static inline void dcfgsg(char *dis, char g);

static inline void
dcfgsa(char *const dis, const char a)
{
	dis[0] = dis[1] = dis[2] = dis[3] = a;
}

static inline void
dcfgsb(char *const dis, const char b)
{
	dis[4] = dis[6] = b;
}

static inline void
dcfgsc(char *const dis, const char c)
{
	dis[5] = dis[7] = c;
}

static inline void
dcfgsd(char *const dis, const char d)
{
	dis[8] = dis[9] = dis[10] = dis[11] = d;
}

static inline void
dcfgse(char *const dis, const char e)
{
	dis[12] = dis[14] = e;
}

static inline void
dcfgsf(char *const dis, const char f)
{
	dis[13] = dis[15] = f;
}

static inline void
dcfgsg(char *const dis, const char g)
{
	dis[16] = dis[17] = dis[18] = dis[19] = g;
}

int main(void)
{
	char dis[20];
	return EXIT_SUCCESS;
}
