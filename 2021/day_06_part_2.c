#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DAYS 256

int main(void)
{
	unsigned long i, st[9], t, ttl;
	unsigned int ds;
	memset(st, 0, sizeof(st));
	while (fscanf(stdin, "%lu ,", &i) != EOF) {
		++st[i];
	}
	for (ds = 0; ds < DAYS; ++ds) {
		t = st[0];
		for (i = 0; i < 8; ++i)
			st[i] = st[i + 1];
		st[8] = t;
		st[6] += st[8];
	}
	ttl = 0;
	for (i = 0; i < 9; ++i)
		ttl += st[i];
	printf("%lu\n", ttl);
	return EXIT_SUCCESS;
}
