#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BITMSK (~(~(0U) << BITWT))
#define BITWT  12U

#define BMC(L, H) ((L) > (H) ? 0x0U : 0x1U)

int main(void)
{
	unsigned int gr, hi[BITWT], lo[BITWT];
	int ch, i;
	memset(hi, 0, sizeof(hi));
	memset(lo, 0, sizeof(lo));
	for (i = 0; (ch = fgetc(stdin)) != EOF; ++i) {
		switch (ch) {
		case '0':
			lo[i] += 1;
			break;
		case '1':
			hi[i] += 1;
			break;
		case '\n':
			i = -1;
			break;
		}
	}
	gr = 0;
	for (i = 0; i < (int)BITWT; ++i) {
		gr <<= 1U;
		gr |= BMC(lo[i], hi[i]);
	}
	printf("%u\n", gr * (~gr & BITMSK));
	return EXIT_SUCCESS;
}
